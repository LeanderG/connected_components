
var distances = []
var data
var groupCounter = 0
var squareReducer = function(v) {
  return v * v
}
var rangeSlider = document.getElementById("myRange")
var xSelect = document.getElementById("x")
var ySelect = document.getElementById("y")
function run(){
    vegaScatter(data);
}

d3.csv("anunan_calls.csv", function(d) {
    data = d
  data.forEach(function (value) {
      for (var key in value) {
        value[key] = +value[key]
      }
  })
  for (var key in data[0]) {
    xSelect.options[xSelect.options.length] = new Option(key, key)
    ySelect.options[ySelect.options.length] = new Option(key, key)
  }

  for (var i = 0; i < data.length - 1; i++) {
    for (var j = i + 1; j < data.length; j++) {

      var distance = 0
      for (var key in data[i]) {
        distance += Math.pow((data[i][key] - data[j][key]), 2)
      }
      distance = Math.sqrt(distance)
      distances.push({
        distance: distance,
        p: i,
        q: j
      })
    }
  }

  distances.sort(function(a, b) {
    return a.distance - b.distance
  })
    rangeSlider.setAttribute("min",Math.round(distances[0].distance))
    rangeSlider.setAttribute("max",Math.round(distances[distances.length-1].distance))
})

function visitEdges(vertex) {
  vertex.group = groupCounter
  vertex.visited = true
   vertex.edges.forEach(function(i){
      var v = data[i]
    if(!v.visited) {
      visitEdges(v)
    }
  })
}

function vegaScatter(data) {
    groupCounter = 0
    data.map(function(d) {
        d.edges = []
        d.visited = false
        d.group = null
    })

    for(var i = 0;i<distances.length;i++){
        if(distances[i].distance > rangeSlider.value) break;
        data[distances[i].q].edges.push(distances[i].p)
        data[distances[i].p].edges.push(distances[i].q)
    }
    data.forEach(function(d){
        if(!d.visited) {
            visitEdges(d)
            groupCounter++
        }
    })

   var yourVlSpec = {
        "$schema": "https://vega.github.io/schema/vega-lite/v2.json",
        "description": "A scatterplot ",
       "width":400,
       "height":400,
        "data": {
            "values": data
        },
        "mark": "circle",
        "encoding": {
            "x": {"field": xSelect.value, "type": "quantitative"},
            "y": {"field": ySelect.value, "type": "quantitative"},
            "color": {"field": "group", "type": "nominal"}
        }
    }

    var opt = {
        mode: "vega-lite",
        actions: false
    };

    var tooltipOption = {
        showAllFields: false,
        fields: [
            { field: xSelect.value },
            { field: ySelect.value },
            { field: "group" }
        ]
    };

    vegaEmbed("#myChart", yourVlSpec, opt).then(function (result) {
        vegaTooltip.vegaLite(result.view, yourVlSpec,tooltipOption);
    }).catch(console.error);
}
